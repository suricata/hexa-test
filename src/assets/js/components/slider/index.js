import Glide from '@glidejs/glide';

import  './style.scss';

const initSlider = () => {
  const glide = new Glide('[data-slider="slider"]', {
    perView: 1,
    gap: 0,
    type: 'carousel',
    focusAt: 'center',
  });

  const sliderBlock = document.querySelector('[data-slider="slider"]');
  const sliderCarts = Array.prototype.slice.call(sliderBlock.querySelectorAll('.glide__slide'));

  sliderCarts.forEach((order, key) => {
    const bullet = document.createElement('button');
    bullet.classList.add('glide__bullet');
    bullet.setAttribute('data-glide-dir', `=${key}`);
    document.querySelector('[data-bullets="slider"]').appendChild(bullet);
  });

  const sliderInfo = [
    {roomType: 'кухни' , price: 50},
    {roomType: 'спальни' , price: 60},
    {roomType: 'детской', price: 70},
    {roomType: 'гостиной', price: 70},
  ];

  glide.on('run', function() {
    const roomType = document.querySelector('[data-type]');
    const priceValue = document.querySelector('[data-numb]');
    roomType.innerHTML = `<span>Для ${sliderInfo[glide.index].roomType}</span>`;
    priceValue.innerHTML = `<span>от</span> ${sliderInfo[glide.index].price} <span>$/м <sup>2</sup></span>`;
  });

  glide.mount();


};

initSlider();